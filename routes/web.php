<?php
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('categories/export/', [CategoryController::class, 'export'])->name('categories.export');

Route::get('categories/pdf/', [CategoryController::class, 'downloadPdf'])->name('categories.pdf');

Route::get('/trashed-categories', [CategoryController::class, 'trash'])->name('categories.trashed');

Route::get('/trashed-categories/{category}/restore', [CategoryController::class, 'restore'])->name('categories.restore');

Route::delete('/trashed-categories/{category}/delete', [CategoryController::class, 'delete'])->name('categories.delete');

Route::resource('categories', CategoryController::class);
